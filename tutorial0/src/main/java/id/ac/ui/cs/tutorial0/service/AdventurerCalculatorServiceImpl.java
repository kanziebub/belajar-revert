package id.ac.ui.cs.tutorial0.service;

import org.springframework.stereotype.Service;

import java.util.Calendar;

@Service
public class AdventurerCalculatorServiceImpl implements AdventurerCalculatorService {

    @Override
    public int countPowerPotensialFromBirthYear(int birthYear) {
        int rawAge = getRawAge(birthYear);
        int power = 0;

        if (rawAge<30) {
            power = rawAge*2000;
        } else if (rawAge <50) {
            power = rawAge*2250;
        } else {
            power =  rawAge*5000;
        }

        return power;
    }

    @Override
    public String powerClassifier(int power) {
        String powClass = "";

        if (power > 0 && power <= 20000) {
            powClass = "C class";
        } else if (power > 20000 && power <= 100000) {
            powClass = "B class";
        } else if (power > 100000) {
            powClass = "A class";
        }
        
        return powClass;
    }

    private int getRawAge(int birthYear) {
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        return currentYear-birthYear;
    }
}
